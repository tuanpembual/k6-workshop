#!/usr/bin/env bash

## Install Deps
#apt update -y && apt install -yqq --no-install-recommends apt-transport-https ca-certificates curl gnupg-agent software-properties-common

## install nginx haproxy
apt install -yqq --no-install-recommends nginx haproxy && apt autoremove -y

## config
cat <<EOF > /var/www/html/index.html
<!DOCTYPE html>
<html>
<head>
<title>K6 Target</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>

<body>

<h1>Welcome to K6</h1>
<p>If you see this page, the web server is successfully installed and
working. Further configuration is required.</p>
</body>
</html>
EOF

sed -i 's/80/8090/g' /etc/nginx/sites-enabled/default
systemctl restart nginx

cat <<EOF >> /etc/haproxy/haproxy.cfg
frontend http_front
   bind *:80
   default_backend http_back

backend http_back
   balance roundrobin
   server server1 127.0.0.1:8090 check

listen stats
    bind *:8080
    stats enable
    stats uri /stats
    stats refresh 2s
EOF

systemctl restart haproxy
