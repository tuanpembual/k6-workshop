#!/usr/bin/env bash

rm -rf target/*
k6 run test.js
sudo cp -f target/summary.html /var/www/html/summary.html
