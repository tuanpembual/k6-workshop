#!/usr/bin/env bash

## Install Deps
apt update -y && apt install -yqq --no-install-recommends apt-transport-https ca-certificates curl gnupg-agent software-properties-common nginx

#tunning systcl
echo "net.ipv4.ip_local_port_range = 1024 65535" >> /etc/sysctl.conf
echo "net.ipv4.tcp_tw_reuse=1" >> /etc/sysctl.conf
echo "net.ipv4.tcp_timestamps=1" >> /etc/sysctl.conf
echo "kernel.pid_max=999999" >> /etc/sysctl.conf
echo "kernel.thread-max=999999" >> /etc/sysctl.conf
echo "vm.max_map_count=999999" >> /etc/sysctl.conf
echo "fs.file-max=999999" >> /etc/sysctl.conf
echo "* - nofile 999999" >> /etc/security/limits.conf
echo "* hard fsize -1" >> /etc/security/limits.conf
echo "root - nofile 999999" >> /etc/security/limits.conf
echo "root hard fsize -1" >> /etc/security/limits.conf
sysctl -p /etc/sysctl.conf .

# Install k6 and nodejs
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C5AD17C747E3415A3642D57D77C6C491D6AC1D69
echo "deb https://dl.k6.io/deb stable main" | tee /etc/apt/sources.list.d/k6.list

curl -fsSL https://deb.nodesource.com/setup_18.x | bash - &&
apt update -y && apt install -yqq --no-install-recommends k6 nodejs && apt autoremove -y
