# README
How to use this repo

# VM target
## prepare target vm to be tested
- create vm with debian base OS
- ssh to vm-target
- update and upgrade
- copy file `install-target.sh` to vm
- run bash as root `sudo su && sh install-target.sh`

# VM K6
## prepare VM k6
- create vm with debian base OS
- ssh to vm-k6
- update and upgrade
- copy file `install-k6.sh` to vm
- run bash as root `sudo su && sh install-k6.sh`

## prepare test script
- as user copy file `install-script.sh`
- run `sh install-script.sh`

# RUN test
- ssh to vm-k6
- run script `cd k6 && sh run.sh`
- waiting report created

# NOTES
- page target can be access by open in browser `ip-public-vm-target/index.html`
- status haproxy can be access by open in browser `ip-public-vm-target:8080/stats`
- report can be access by open in browser `ip-public-vm-k6/summary.html`

# TBD
