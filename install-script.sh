#!/usr/bin/env bash
mkdir -p k6/target
cat <<EOF > k6/test.js
import http from 'k6/http';
import { check, group, sleep } from 'k6';
import { htmlReport } from 'https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js';

export function handleSummary(data) {
  return {
    'target/summary.html': htmlReport(data),
    'target/summary.json': JSON.stringify(data),
  };
}

export const options = {
  scenarios: {
    Test: {
      executor: 'per-vu-iterations',
      vus: 20,
      iterations: 100,
      maxDuration: '90s',
      exec: 'test',
    },
  },
};

export function test() {
  group('Test 1', () => {
    const params = {
      timeout: '60s',
    };

    const response = http.get('http://13.229.181.124/index.html', params);
    check(response, {
      'Test URL 1 Success': () => response.status.toString() === '200',
    });
    sleep(0.5);
  });
}
EOF

cat <<EOF > k6/run.sh
#!/usr/bin/env bash

rm -rf target/*
k6 run test.js
sudo cp -f target/summary.html /var/www/html/summary.html
EOF
